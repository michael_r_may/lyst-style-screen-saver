//
//  Created by Developer on 2015/04/13.
//  Copyright (c) 2015 Michael May. All rights reserved.
//

#import "LystScreenSaverView.h"
#import "LYCGFloatSizeHelper.h"

@implementation LystScreenSaverView

- (instancetype)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview
{
    self = [super initWithFrame:frame isPreview:isPreview];
    
    if (self) {
        [self setAnimationTimeInterval:1/30.0];
    }
    
    return self;
}

//- (void)startAnimation
//{
//    [super startAnimation];
//}
//
//- (void)stopAnimation
//{
//    [super stopAnimation];
//}

-(NSColor*)backgroundColor
{
    return [NSColor colorWithCalibratedRed:0.0f
                                     green:0.0f
                                      blue:0.0f
                                     alpha:1.0f];
}

-(NSColor*)foregroundColor
{
    return [NSColor colorWithCalibratedRed:1.0f
                                     green:1.0f
                                      blue:1.0f
                                     alpha:1.0f];
}

-(NSFont*)font
{
    NSRect frame = self.bounds;

    CGFloat fontSize = round64Helper(frame.size.width / 8.0f);

    NSFont *font = [NSFont fontWithName:@"Brown-Bold" size:fontSize];
    if(font == nil) {font = [NSFont fontWithName:@"Avenir" size:fontSize];}
    
    return font;
}

-(NSDictionary*)attributesForTimeDrawing
{
    return @{NSFontAttributeName : [self font],
             NSForegroundColorAttributeName : [self foregroundColor],
             NSBackgroundColorAttributeName : [self backgroundColor]};
    
}

-(NSRect)timeDrawingFrame
{
    NSRect frame = self.bounds;
    
    NSDate *timeWithAllZeroes = [NSDate dateWithTimeIntervalSince1970:0];
    NSString *timeString = [self timeStringWithDate:timeWithAllZeroes];
    NSDictionary *attributes = [self attributesForTimeDrawing];
    CGSize size = [timeString sizeWithAttributes:attributes];
    NSRect dateFrame = frame;
    
    dateFrame.origin.x = round64Helper((frame.size.width - size.width) / 2.0f);
    dateFrame.origin.y = round64Helper((frame.size.height - size.height) / 2.0f);
    dateFrame.size = size;
    
    return dateFrame;
}

-(void)drawRect:(NSRect)rect
{
    [super drawRect:rect];
    
    NSRect frame = [self bounds];
    
    {
        NSColor *color = [self backgroundColor];
        [color set];
        
        NSBezierPath *path = [NSBezierPath bezierPathWithRect:frame];
        [path fill];
    }
    
    {
        NSColor *color = [self foregroundColor];
        [color set];
        
        CGFloat lineWidth = round64Helper(frame.size.width / 100.0f);
        
        NSRect frameToStroke = NSInsetRect(frame, 2 * lineWidth, 2 * lineWidth);
        NSBezierPath *path = [NSBezierPath bezierPathWithRect:frameToStroke];
        
        [path setLineWidth:lineWidth];
        
        [path stroke];
    }
}

// remember that 0,0 is bottom left
- (void)animateOneFrame
{
    
    /*
    NSRect frame = [self bounds];
    
    {
        NSColor *color = [self backgroundColor];
        [color set];
        
        NSBezierPath *path = [NSBezierPath bezierPathWithRect:frame];
        [path fill];
    }

    {
        NSColor *color = [self foregroundColor];
        [color set];
        
        CGFloat lineWidth = round64Helper(frame.size.width / 100.0f);
        
        NSRect frameToStroke = NSInsetRect(frame, 2 * lineWidth, 2 * lineWidth);
        NSBezierPath *path = [NSBezierPath bezierPathWithRect:frameToStroke];
        
        [path setLineWidth:lineWidth];
        
        [path stroke];
    }
    */
    
//    {
//        NSRect lystLine;
//        lystLine.origin.x = round64Helper(frame.sze.width / 15.0f);
//        lystLine.origin.y = round64Helper(frame.size.height / 10.0f);
//        lystLine.size.width = round64Helper(frame.size.width / 5.0f);
//        lystLine.size.height = round64Helper(frame.size.height / 50.0f);
//        
//        NSBezierPath *path = [NSBezierPath bezierPathWithRect:lystLine];
//        [path fill];
//    }
    
    {
        NSDictionary *attributes = @{NSFontAttributeName : [self font],
                                     NSForegroundColorAttributeName : [self foregroundColor],
                                     NSBackgroundColorAttributeName : [self backgroundColor]};
        
        NSRect dateFrame = [self timeDrawingFrame];
        
        NSColor *backgroundColor = [self backgroundColor];
        [backgroundColor set];
        
        NSBezierPath *path = [NSBezierPath bezierPathWithRect:dateFrame];
        [path fill];
        
        NSColor *foregroundColor = [self foregroundColor];
        [foregroundColor set];
        
        NSString *timeString = [self timeString];
        [timeString drawInRect:dateFrame withAttributes:attributes];
    }
}

- (BOOL)hasConfigureSheet
{
    return NO;
}

- (NSWindow*)configureSheet
{
    return nil;
}

#pragma mark - 

-(NSString*)timeStringWithDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeStyle = NSDateFormatterMediumStyle;
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    
    NSString *timeString = [dateFormatter stringFromDate:date];
    return timeString;
}

-(NSString*)timeString
{
    NSDate *date = [NSDate date];
    
    return [self timeStringWithDate:date];
}

@end
